#!/usr/bin/env bash

jfrog rt dl generic-dev-local/*.zip ./generic/ --build-name=generic-pipe-example --build-number=1
jfrog rt u generic/test.zip generic-dev-local/ --build-name=generic-pipe-example --build-number=1
jfrog rt bce generic-pipe-example 1
jfrog rt bag generic-pipe-example 1 --config ./jira-cli.conf
jfrog rt bp generic-pipe-example 1

